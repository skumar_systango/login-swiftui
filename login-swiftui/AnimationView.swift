//
//  AnimationView.swift
//  DemoLogin
//
//  Created by macmini21 on 23/02/23.
//

import SwiftUI

struct AnimationView: View {
    @State var animation2 = false
    @State var animation1 = false
    var body: some View {
        ZStack {
            Rectangle()
                .frame (width: animation1 ? 40 : 200, height: 40)
                .cornerRadius(animation1 ? 20 : 8)
                .foregroundColor(Color.blue)
                .animation(Animation.linear (duration: 0.5))
            if animation2 {
                Circle()
                    .trim(from: 0, to:animation2 ? 0.2 : 0)
                    .stroke(lineWidth: 8)
                    .frame (width: 40, height: 40)
                animation (Animation.linear (duration: 0.5) .delay (0.5))
                    .onAppear {
                        self.animation2 = true
                    }.onTapGesture {
                        self.animation1 = true
                    }
            }
        }
        
    }
}
struct AnimationView_Previews: PreviewProvider {
    static var previews: some View {
        AnimationView()
    }
}
