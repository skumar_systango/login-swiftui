//
//  HomeView.swift
//  DemoLogin
//
//  Created by macmini21 on 24/02/23.
//

import SwiftUI

struct HomeView: View {
    let icons: [String] = ["img1", "img2", "img3", "img4"]
    
//    init() {
//        UITabBar.appearance().unselectedItemTintColor = .red
//        UITabBar.appearance().backgroundColor = .red
//        UITableView.appearance().separatorStyle = .none
//        UITableViewCell.appearance().backgroundColor = .clear
//        UITableView.appearance().backgroundColor = .clear
//    }
    
    
    var body: some View {
        
        ZStack {
            Color.white.opacity(0.2).ignoresSafeArea()
            VStack( spacing: 0) {
                Text("Hello \n Sunil Welcome back!")
                    .multilineTextAlignment(.center)
                    .foregroundColor(.blue)
                    .font(.title)
                TabView {
                    ForEach(icons, id: \.self) { icon in
                        Image(icon)
                            .resizable()
                            .background(Color.blue)
                            .cornerRadius(20)
                            .padding()
                            .shadow(color: .gray.opacity(0.2), radius: 10, x: 0, y: 0)
                    }
                }
                .accentColor(.red)
                .frame(height:200)
                .tabViewStyle(PageTabViewStyle())
                .padding(.bottom, 0)
                
                .cornerRadius(20)
                List {
                    ForEach(0..<20) {
                        index in
                        ProductItemVIew()
                    }.listRowBackground(Color.clear)
                    
                }
                .padding(.top, 0)
            }
        }
//        .navigationTitle("Products")
//        .navigationBarTitleDisplayMode(.inline)
//        .navigationBarBackButtonHidden(true)
        .navigationBarHidden(true)
        
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
