//
//  ForgotPasswordView.swift
//  DemoLogin
//
//  Created by Sunil Kumar on 23/02/2023.
//

import SwiftUI

struct ForgotPasswordView: View {
    @State var mobileEmail: String = ""
    @Environment(\.presentationMode) var presentationMode
    var body: some View {
        ZStack {
            Color.blue.ignoresSafeArea()
             VStack(alignment: .center, spacing: 10) {
                Text("Forgot Passowrd")
                    .font(.title)
                    .foregroundColor(.blue)
                    .fontWeight(.bold)
                    .padding()
                 emailView
                 loginview
                 HStack {
                     Text("back to login?")
                     Button {
                         presentationMode.wrappedValue.dismiss()
                     } label: {
                         Text("Login")
                             .foregroundColor(.blue)
                             .font(.subheadline)
                             .underline()
                     }
                  }
                 .padding(.top, 0)
                 .padding(.bottom, 10)
             }
            .background(Color.white)
            .cornerRadius(20)
            .padding()
        } .navigationBarHidden(true)
    }
    var emailView: some View {
        HStack {
            Image(systemName: "envelope.fill")
                .foregroundColor(.blue)
                .frame(width: 18, height: 18, alignment: .center)
                .padding(.leading)
            TextField("Email or Mobile", text: $mobileEmail)
        }
        .frame( height: 45)
        .overlay(
            RoundedRectangle(cornerRadius: 22.5)
                .stroke(Color.blue, lineWidth: 1)
        )
        .padding(.horizontal, 20)
        .padding(.vertical, 5)
    }
    
    var loginview: some View {
        Button {
        } label: {
             Text("Submit")
            .frame(width: 250, height: 50)
                .background(Color.blue)
                .foregroundColor(.white)
                .cornerRadius(25)
                .padding(.top, 10)
        }
    }
}

struct ForgotPasswordView_Previews: PreviewProvider {
    static var previews: some View {
        ForgotPasswordView()
    }
}
 
 
