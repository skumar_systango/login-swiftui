//
//  ProductItemVIew.swift
//  DemoLogin
//
//  Created by macmini21 on 24/02/23.
//

import SwiftUI

struct ProductItemVIew: View {
    var body: some View {
        HStack {
            Image("mobile")
                .resizable()
                .frame(width: 105, height: 120)
                .clipShape(Rectangle())
                .scaledToFit()
                .padding(0)
            VStack(spacing: 5) {
                Text("Apple iPhone 13 Mini 128 GB, Midnight (Black)(491997714)")
                    .multilineTextAlignment(.leading)
                    .padding(.leading, 0)
                    .font(.system(size: 15))
                    .foregroundColor(.black)
                
                HStack(spacing:0){
                    Text("₹")
                        .font(.system(size: 12))
                    Text("80,678")
                        .font(.system(size: 15))
                    Spacer()
                }
                
                HStack{
                    Text("Save ₹160 ")
                        .font(.system(size: 13))
                        .padding(2)
                        .background(.green)
                    
                    Text("with cupon")
                    Spacer()
                }
                
                HStack {
                    Text("Get it by ")
                        .foregroundColor(.gray.opacity(0.5))
                        .font(.system(size: 13))
                    Text("Thursday 28 febuarry")
                        .font(.system(size: 14))
                        .foregroundColor(.black.opacity(0.8))
                    Spacer()
                }
                Text("Free delivery by us here")
                    .padding(.leading, 0)
                    .font(.system(size: 14))
                    .foregroundColor(.black.opacity(0.8))
                    .padding(0)
                Spacer()
            }
            
        }.frame(width: .infinity, height: 150)
            .shadow(color: .gray.opacity(0.2), radius: 10, x: 0, y: 0)

    }
}

struct ProductItemVIew_Previews: PreviewProvider {
    static var previews: some View {
        ProductItemVIew()
    }
}
