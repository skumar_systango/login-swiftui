//
//  DemoLoginApp.swift
//  DemoLogin
//
//  Created by Sunil Kumar on 22/02/2023.
//

import SwiftUI

@main
struct DemoLoginApp: App {
    var body: some Scene {
        WindowGroup {
            LoginView()
        }
    }
}
