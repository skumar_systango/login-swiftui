//
//  LoginView.swift
//  DemoLogin
//
//  Created by Sunil Kumar on 22/02/2023.
//

import SwiftUI

struct LoginView: View {
    @State var mobileEmail: String = ""
    @State var password: String = ""
    
 
    var body: some View {
        NavigationView {
            ZStack {
                Color.blue.ignoresSafeArea()
                VStack {
                   Text("Welcome Back :)")
                        .font(.title)
                        .foregroundColor(.white)
                        .padding(.top, 20)
                    Spacer()
                }
                .padding()
               
                VStack(alignment: .center, spacing: 10) {
                    Text("Login")
                        .font(.title)
                        .foregroundColor(.blue)
                        .fontWeight(.bold)
                        .padding()
                    
                    VStack {
                        emailView //Email fileds
                        if !mobileEmail.isEmpty && !mobileEmail.isValidEmail() {
                            Text("Please enter valid email id")
                                .frame(maxWidth: .infinity, alignment: .leading)
                                .padding(.leading, 22)
                                .padding(.top, -8)
                                .foregroundColor(.red)
                                .font(.system(size: 15))
                        }
                        
                    }
                    
                    
                    VStack {
                        passwordView //Password field
                        if password.count < 7 && !password.isEmpty{
                            Text("Please enter password at least 7 characters")
                                .frame(maxWidth: .infinity, alignment: .leading)
                                .padding(.leading, 22)
                                .padding(.top, -8)
                                .foregroundColor(.red)
                                .font(.system(size: 15))
                        }
                        
                            
                    }
                      forgotPasswordView//Forgot Password buttom
                        .padding(.leading, 20)
                        .padding(.top, -10)
                     
                    loginview//login button
                    signUpview //Sign Up Buttom
                  
                }
                .background(Color.white)
                .cornerRadius(20)
                .padding()
                
            }
            .overlay(alignment: .bottomTrailing) {
                Image("top_bg")
                    .resizable()
                    .frame(width: 150, height: 150, alignment: .bottomTrailing)
                    .padding()
            }.navigationTitle("login")
                .navigationBarHidden(true)
                .background(Color.blue)
        }
        
    }
    
    var emailView: some View {
        HStack {
            Image(systemName: "envelope.fill")
                .foregroundColor(.blue)
                .frame(width: 18, height: 18, alignment: .center)
                .padding(.leading)
            TextField("Email", text: $mobileEmail)
                .keyboardType(.emailAddress)
        } .frame( height: 45)
            .overlay(
                RoundedRectangle(cornerRadius: 22.5)
                    .stroke(Color.blue, lineWidth: 1)
            )
            .padding(.horizontal, 20)
            .padding(.vertical, 5)
    }
    
    var passwordView: some View {
        HStack {
            Image(systemName: "lock.fill")
                .resizable()
                .foregroundColor(.blue)
                .frame(width: 18, height: 18)
                .padding(.leading)
            TextField("Password", text: $password)
                .padding(.leading, 5)
        }
        .frame(height: 45)
        .overlay(
            RoundedRectangle(cornerRadius: 22.5)
                .stroke(Color.blue, lineWidth: 1)
        )
        .padding(.horizontal, 20)
        .padding(.vertical, 5)
    }
    
    var forgotPasswordView: some View {
        HStack {
        NavigationLink {
            ForgotPasswordView()
        } label: {
             Text("Forgot Passowrd")
                .font(.headline)
                .underline()
         }
            Spacer()
        }
    }
    
    var signUpview: some View {
        NavigationLink {
            RegisterView()
        } label: {
             Text("Sign UP")
            .frame(width: 250, height: 50)
                .background(Color.blue)
                .foregroundColor(.white)
                .cornerRadius(25)
                .padding(.top, 5)
                .padding(.bottom, 30)
            
                 
        }
    }
   
    
    var loginview: some View {
        NavigationLink {
            HomeView()
        } label: {
            Text("Login")
                .frame(width: 250, height: 50)
                .background( isFiledsValid() ? Color.blue : Color.blue.opacity(0.4))
                .foregroundColor(.white)
                .cornerRadius(25)
                .padding(.top, 10)
                .padding(.bottom, 5)
            
        }.disabled(!isFiledsValid())
    }
    
     
    
    func isFiledsValid() -> Bool {
        let isNotEmpty = !mobileEmail.isEmpty && !password.isEmpty
        let isValid =  mobileEmail.isValidEmail() && password.count > 6
        return isNotEmpty && isValid
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}
extension String {
    func isValidEmail()->Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
}
