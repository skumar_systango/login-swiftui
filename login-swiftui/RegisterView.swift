//
//  RegisterView.swift
//  DemoLogin
//
//  Created by macmini21 on 22/02/23.
//

import SwiftUI

struct RegisterView: View {
    
    @Environment(\.presentationMode) var presentationMode
    
    @State var name: String = ""
    @State var mobileNumber: String = ""
    @State var email: String = ""
    @State var city: String = ""
    @State var state: String = ""
    @State var country: String = ""
    @State var address: String = ""
    

    var body: some View {
        ZStack {
            Color.blue.ignoresSafeArea()
           
            ScrollView {
                VStack(spacing: 10)  {
                    Spacer()
                    nameView
                    mobileNumverView
                    emailView
                    addressView
                    cityView
                    stateView
                    countryView
                    Button {
                        
                    } label: {
                        Text("Sign Up")
                            .font(.headline)
                            .foregroundColor(.white)
                    }.frame(width: 200, height: 45)
                        .background(Color.blue)
                        .cornerRadius(25)
                        .padding(.bottom, 0)
                        .padding(.top, 20)
                    
                    HStack {
                        Text("Already have an account?")
                        Button {
                            print("back to previous screen")
                            presentationMode.wrappedValue.dismiss()
                        } label: {
                            Text("Login")
                                .foregroundColor(.blue)
                                .font(.subheadline)
                                .underline()
                        }
                     }
                    .navigationTitle("Regiser")
                    .navigationBarTitleDisplayMode(.inline)
                }.padding(.top, 30)
             }
            .background(.white)
            .cornerRadius(radius: 35.0, corners: [.topLeft, .topRight])
            .padding(.top, 150)
            
        }.ignoresSafeArea()
        
    }
    
}

struct RegisterView_Previews: PreviewProvider {
    static var previews: some View {
        RegisterView()
    }
}


extension RegisterView {
    
    
    var nameView: some View {
        HStack {
            Image(systemName: "person.fill")
                .resizable()
                .foregroundColor(.blue)
                .frame(width: 18, height: 18)
                .padding(.leading, 15)
            TextField("Enter your name", text: $name)
                .foregroundColor(.blue)
        }
        .frame(height: 45)
        .overlay(
            RoundedRectangle(cornerRadius: 22.5)
                .stroke(Color.blue, lineWidth: 1)
        )
        .padding(.leading, 20)
        .padding(.trailing, 20)
        .padding(.vertical, 5)
    }
    
    
    var mobileNumverView: some View {
        HStack {
            Image(systemName: "person.fill")
                .resizable()
                .foregroundColor(.blue)
                .frame(width: 18, height: 18)
                .padding(.leading, 15)
            TextField("Enter your mobile number", text: $mobileNumber)
                .foregroundColor(.blue)
                .keyboardType(.numberPad)
        }
        .frame(height: 45)
        .overlay(
            RoundedRectangle(cornerRadius: 22.5)
                .stroke(Color.blue, lineWidth: 1)
        )
        .padding(.leading, 20)
        .padding(.trailing, 20)
        .padding(.vertical, 5)
    }
    
    var emailView: some View {
        HStack {
            Image(systemName: "person.fill")
                .resizable()
                .foregroundColor(.blue)
                .frame(width: 18, height: 18, alignment: .center)
                .padding(.leading, 15)
            TextField("Enter your Email", text: $email)
                .foregroundColor(.blue)
                .keyboardType(.emailAddress)
        }
        .frame(height: 45)
        .overlay(
            RoundedRectangle(cornerRadius: 22.5)
                .stroke(Color.blue, lineWidth: 1)
        )
        .padding(.leading, 20)
        .padding(.trailing, 20)
        .padding(.vertical, 5)
    }
    
    var addressView: some View {
        HStack {
            Image(systemName: "person.fill")
                .resizable()
                .foregroundColor(.blue)
                .frame(width: 18, height: 18, alignment: .center)
                .padding(.leading, 15)
            TextField("Enter your Address", text: $address)
                .foregroundColor(.blue)
        }.frame(height: 45)
            .overlay(
                RoundedRectangle(cornerRadius: 22.5)
                    .stroke(Color.blue, lineWidth: 1)
            )
            .padding(.leading, 20)
            .padding(.trailing, 20)
            .padding(.vertical, 5)
    }
    
    var cityView: some View {
        HStack {
            Image(systemName: "person.fill")
                .resizable()
                .foregroundColor(.blue)
                .frame(width: 18, height: 18, alignment: .center)
                .padding(.leading, 15)
            TextField("Enter your City", text: $city)
                .foregroundColor(.blue)
        }.frame( height: 45)
            .overlay(
                RoundedRectangle(cornerRadius: 22.5)
                    .stroke(Color.blue, lineWidth: 1)
            )
            .padding(.leading, 20)
            .padding(.trailing, 20)
            .padding(.vertical, 5)
    }
    
    var stateView: some View {
        HStack {
            Image(systemName: "person.fill")
                .resizable()
                .foregroundColor(.blue)
                .frame(width: 18, height: 18, alignment: .center)
                .padding(.leading, 15)
            TextField("Enter your City", text: $state)
                .foregroundColor(.blue)
        }.frame( height: 45)
            .overlay(
                RoundedRectangle(cornerRadius: 22.5)
                    .stroke(Color.blue, lineWidth: 1)
            )
            .padding(.leading, 20)
            .padding(.trailing, 20)
            .padding(.vertical, 5)
    }
    
    var countryView: some View {
         HStack {
            Image(systemName: "person.fill")
                .resizable()
                .foregroundColor(.blue)
                .frame(width: 22, height: 22, alignment: .center)
                .padding(.leading, 15)
             TextField("Enter your Country", text: $country)
                 .foregroundColor(.blue)
        }.frame(height: 45)
            .overlay(
                RoundedRectangle(cornerRadius: 22.5)
                    .stroke(Color.blue, lineWidth: 1)
            )
            .padding(.leading, 20)
            .padding(.trailing, 20)
            .padding(.vertical, 5)
    }
    
}
